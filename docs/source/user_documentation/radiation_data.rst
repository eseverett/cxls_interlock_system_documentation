.. these roles are defined to use custom css classes
.. role:: white-cell


Ionizing Radiation Surveys and Results
======================================

This section provides a summary of the radiation data that has been collected in the CXLS suite. 

-----


Routine Radiation Survey of Vault-1 and Beam Line
-------------------------------------------------

Radiation surveys of the CXLS beamline and Vault-1 are taken routinely every week and after the accelerator is run.
Below are maps of the locations where radiation measurements are taken using the Ludlum 9DP.

.. figure:: /images/radiation_survey/beamline_map.JPG
   :align: center
   
   **Figure 1:** Survey points along the beamline. 

.. figure:: /images/radiation_survey/Vault-1_map.JPG
   :align: center
   
   **Figure 2:** Survey points throughout Vault-1.

.. figure:: /images/radiation_survey/Accelerator_Lab_map.JPG
   :align: center
   
   **Figure 3:** Survey point in the Accelerator Lab for reference. 


.. .. csv-table:: Radiation survey reading on contact with beamline.
..     :file: /csv/contact.csv
..     :align: center
..     :header-rows: 1

.. .. csv-table:: Radiation survey readings at 30 cm from beamline.
..     :file: /csv/distance.csv
..     :align: center
..     :header-rows: 1

.. .. csv-table:: Radiation survey in the general area of Vault-1. 
..     :file: /csv/area.csv
..     :align: center
..     :header-rows: 1


Below is a summary of the data collected from the radiation surveys. 

.. figure:: /csv/contact_distance_average.png
   :align: center
   
   **Figure 4:** Average radiation survey readings at contact and 30 cm from beamline.

.. figure:: /csv/area_average.png
   :align: center
   
   **Figure 5:** Average radiation survey readings in the general area of Vault-1.

.. figure:: /csv/dose_rate_distribution.png
   :align: center
   
   **Figure 6:** Dose rate distribution in Vault-1.
   Note that there is one point that was taken after a long beam time near 1200, however the vast majority of the points are below 20 uSv/hr.


-----


Radiation Characterization Survey of Vault-1
--------------------------------------------

After the Apantec area monitors were installed, and the software was developed to communicate and collect data from these devices, a survey was taken of Vault-1 and perimeter areas. 
The Apantec area monitor in Vault-1 was made mobile and placed around various locations in Vault-1. 
Below are the map of the detector locations and the summary of the data. 


.. figure:: /images/radiation_survey/Vault-1_cart_overlay.png
   :align: center
   
   **Figure 7:** This is the map of the locations where Vault-1 radiation meter was placed.


.. list-table::
   :align: center

   * - .. image:: /images/radiation_survey/Vault-1_XELERA_Overlay.png
         :align: center
     
     - .. image:: /images/radiation_survey/XELERA_20pC_compare.png
         :align: center
     
     - .. image:: /images/radiation_survey/XELERA_200pC_compare.png
         :align: center

   * - Overlay of the XELERA simulations with the cart positions used in the survey.  :white-cell:`==============`
     - Comparison of the XELERA simulations with the actual measurements at 20 pC.  :white-cell:`================`
     - Comparison of the XELERA simulations with the actual measurements at 200 pC.  :white-cell:`===============`


.. table-caption::
   **Figure 8:** This figure shows the XELERA simulation data compared to the measured results. 


.. list-table::
   :align: center

   * - .. image:: /images/radiation_survey/Vault-1_readings_1.png
         :align: center
         :scale: 45 %
     
     - .. image:: /images/radiation_survey/Vault-1_readings_2.png
         :align: center
         :scale: 45 %
     
     - .. image:: /images/radiation_survey/Vault-1_readings_3.png
         :align: center
         :scale: 45 %

     - .. image:: /images/radiation_survey/Vault-1_readings_4.png
         :align: center
         :scale: 45 %

     - .. image:: /images/radiation_survey/Vault-1_readings_5.png
         :align: center
         :scale: 45 %

     - .. image:: /images/radiation_survey/Vault-1_readings_6.png
         :align: center 
         :scale: 45 %

   * - Radiation survey readings at Vault-1. :white-cell:`=======================================================`
     - Radiation survey readings at Vault-1. :white-cell:`=======================================================`
     - Radiation survey readings at Vault-1. :white-cell:`=======================================================`
     - Radiation survey readings at Vault-1. :white-cell:`=======================================================`
     - Radiation survey readings at Vault-1. :white-cell:`=======================================================`
     - Radiation survey readings at Vault-1. :white-cell:`=======================================================`


.. table-caption::
   **Figure 9:** This figure shows the average radiation gamma and neutron reading at different locations and configurations, as well as the projections to full beam charge specs.


-----



Radiation Characterization Survey of Hutch-1
--------------------------------------------

Once holes were put into the Hutch-1 / Vault-1 wall for the divergent and collimated beamline, a survey was taken of Hutch-1.
Inside of Hutch-1, radiation measurements were taken near the penetration, inside of the experimental chamber, and behind the detector chamber.
Below are the maps of the detectors, the beam charge and energy, and the summary of the data that was collected and measured above background.  


.. figure:: /images/radiation_survey/Hutch-1_sensor_positions_1.png
   :align: center
   
   **Figure 10:** This is the map of the locations where Hutch-1 radiation meter was placed.

.. figure:: /images/radiation_survey/Hutch-1_sensor_positions_2.png
   :align: center
   
   **Figure 11:** This is the map of the locations where Hutch-1 radiation meter was placed.

.. figure:: /images/radiation_survey/Hutch-1_beam_levels.png
   :align: center
   
   **Figure 12:** This is the map of the beam levels in Hutch-1.

.. list-table::
    :align: center 

    * - .. image:: /images/radiation_survey/Hutch-1_readings_1.png
          :align: center
          :scale: 90 %

      - .. image:: /images/radiation_survey/Hutch-1_readings_2.png
          :align: center
          :scale: 90 %

      - .. image:: /images/radiation_survey/Hutch-1_readings_3.png
          :align: center
          :scale: 90 %

      - .. image:: /images/radiation_survey/Hutch-1_readings_4.png
          :align: center 
          :scale: 90 %
    
    * - Radiation in Hutch-1 near the penetration points. Spikes when downstream pop screens are in and a shutter is open.
      - Radiation in Hutch-1 behind the detector chamber. Spikes when POP10 is in and collimated shutter open.
      - Radiation in Hutch-1 near the penetration points. Spikes when downstream pop screens are in and a shutter is open. 
      - Radiation in Hutch-1 inside the experiment chamber. Spikes when downstream pop screens are in and collimated shutter is open. 


.. table-caption::
   **Figure 13:** This figure is showing the only data where above background radiation was measured.