Radiation Survey Equipment Calibration Check
============================================

Below is the calibration data for all detection equipment used in the CXLS interlocks system. 

-----

Apantec Monitors
----------------

.. csv-table::
    :file: /csv/apantec_area_monitors.csv
    :align: center 
    :header-rows: 1

-----


Ludlum Monitors
---------------

.. csv-table::
    :file: /csv/ludlum_monitors.csv
    :align: center
    :header-rows: 1

.. pdfbutton:: M9DP_&_M9DPstar.pdf

.. pdfbutton:: M2363.pdf

.. pdfbutton:: M42-41_&_M42-41L.pdf

.. pdfbutton:: M23_&_23-1.pdf


-----

O2 Monitors
-----------

.. csv-table::
    :file: /csv/O2_monitors.csv
    :align: center
    :header-rows: 1

.. pdfbutton:: Manual-RAD-0002-Oxygen-Alarm-web.pdf


-----

Narda Monitors
------------------

.. csv-table::
    :file: /csv/microwave_monitors.csv
    :align: center
    :header-rows: 1

.. pdfbutton:: Manual_SMARTS_II.pdf
